<?php

/*

  Resets left and right MPTT pointers. This fixes issues where created albums have the wrong parents

  The script creates temporary MEMORY table to do all the heavy lifting in to improve performance. It
  should be allowed to run uninterrupted, otherwise the pointer table will be broken

  The script will assign proper ptr values to the memory table. Afterwards the memory table should be 
  checked for proper pointer assignement and then merged with items table using db query at bottom

*/

    $link = mysql_connect('127.0.0.1', 'user', 'password');
    mysql_select_db('racesgallery');
    mysql_query("SET max_heap_table_size = 1024 * 1024 * 1024");

    mysql_query("CREATE TABLE IF NOT EXISTS `tmp_tree` (
        `id`        char(36) NOT NULL DEFAULT '',
        `parent_id` char(36)          DEFAULT NULL,
        `left_ptr`       int(11)  unsigned DEFAULT NULL,
        `right_ptr`      int(11)  unsigned DEFAULT NULL,
        PRIMARY KEY      (`id`),
        INDEX USING HASH (`parent_id`),
        INDEX USING HASH (`left_ptr`),
        INDEX USING HASH (`right_ptr`)
    ) ENGINE = MEMORY
    SELECT `id`,
           `parent_id`,
           `left_ptr`,
           `right_ptr`
    FROM   `items`");

    // Start by setting all pointer values to empty
    mysql_query("UPDATE `tmp_tree` SET `left_ptr`  = NULL, `right_ptr` = NULL");

    //Establishing starting numbers for all root elements.
    $nullptrs = mysql_query("SELECT id FROM `tmp_tree` WHERE `parent_id` = 0 AND `left_ptr` IS NULL AND `right_ptr` IS NULL");
    $startId = 1;

    while($result = mysql_fetch_object($nullptrs)) {
        print "id = " . $result->id . "\n";
        mysql_query("UPDATE `tmp_tree` SET `left_ptr`  = " . $startId . ", `right_ptr` = " . ($startId + 1) . " WHERE id = " . $result->id);
        $startId = $startId + 2;
    }
    //Switching the indexes for the left_ptr/right_ptr columns to B-Trees to speed up the next section, which uses range queries.
    mysql_query("DROP INDEX `left_ptr`  ON `tmp_tree`");
    mysql_query("DROP INDEX `right_ptr` ON `tmp_tree`");
    mysql_query("CREATE INDEX `left_ptr`  USING BTREE ON `tmp_tree` (`left_ptr`)");
    mysql_query("CREATE INDEX `right_ptr` USING BTREE ON `tmp_tree` (`right_ptr`)");

        # Picking an unprocessed element which has a processed parent.
    //$leftptr = mysql_query("SELECT t.id, t.parent_id FROM `tmp_tree` t INNER JOIN `tmp_tree` p ON t.parent_id = p.id WHERE t.left_ptr IS NULL AND p.left_ptr IS NOT NULL");
    $leftptr = mysql_query("SELECT id, parent_id FROM `tmp_tree` WHERE left_ptr IS NULL ORDER BY id ASC");

    while($result = mysql_fetch_object($leftptr)) {

        $currentId = $result->id;
        $currentParentId = $result->parent_id;
        print "nid = " . $result->id . "\n";

        # Finding the parent's left_ptr value.
        $currentLeft = mysql_result(mysql_query("SELECT  `left_ptr` FROM `tmp_tree` WHERE `id` = " . $currentParentId), 0);

        if($currentLeft) {
          # Shifting all elements to the right of the current element 2 to the right.
          mysql_query("UPDATE `tmp_tree` SET `right_ptr` = `right_ptr` + 2 WHERE  `right_ptr` > " . $currentLeft);
          mysql_query("UPDATE `tmp_tree` SET `left_ptr` = `left_ptr` + 2 WHERE `left_ptr` > " . $currentLeft);
          print "pid = " . $currentParentId . "\n";
          print "left = " . $currentLeft. "\n";

          # Setting left_ptr and right_ptr values for current element.
          mysql_query("UPDATE `tmp_tree` SET `left_ptr`  = " . ($currentLeft + 1) . " , `right_ptr` = " . ($currentLeft + 2) . " WHERE `id` = " . $currentId);
        }  
    }    
    
    /*
    When finished, check the tmp_tree table and then
    write merge with items table using this query

    UPDATE `items`, `tmp_tree`
    SET    `items`.`left_ptr`  = `tmp_tree`.`left_ptr`,
           `items`.`right_ptr` = `tmp_tree`.`right_ptr`
    WHERE  `items`.`id`   = `tmp_tree`.`id`;

    */
?>    
