<?php

    /* This script calculates the relative_path_cache and relative_url_cache
       values. This fixes issues that caused wrong album url and highlight image */
    
    $link = mysql_connect('127.0.0.1', 'user', 'password');
    mysql_select_db('racesgallery');

    $items = mysql_query("SELECT t.id, t.name, t.left_ptr, t.right_ptr, t.slug, p.id AS pid, p.relative_path_cache AS ppath , p.relative_url_cache AS purl FROM items3 t JOIN items3 p ON t.parent_id = p.id WHERE t.id > 1 ORDER BY t.id ASC");
    $rootslugs = array();
  
    while($item = mysql_fetch_object($items)) {
  
      print "ID $item->id PID $item->pid NAME $item->name SLUG $item->slug PARENT PATH CACHE $item->ppath PARENT CACHE URL $item->purl \n";
      if($item->pid == 1) {
        // Calculate the top level items first
        $update = "UPDATE items3 SET relative_path_cache = '" . urldecode($item->name) . "', relative_url_cache = '" . urldecode($item->slug) . "' WHERE id = " . $item->id;
        print $update . "\n";
        mysql_query($update);
        $rootslugs[] = $item->slug;
      } else if(!in_array(array_shift(explode('/', $item->purl)), $rootslugs)) {

        // Sometimes the parent is read before it is properly assigned.  
        // If so, we resort to slower but safer way of calculating
        $names = array();
        $slugs = array();
        $parents = mysql_query("SELECT name, slug FROM items WHERE left_ptr <= " . $item->left_ptr . " AND right_ptr  >= " . $item->right_ptr . " ORDER BY left_ptr ASC");
        while($parent = mysql_fetch_object($parents)) {
          $names[] = rawurlencode($parent->name);
          $slugs[] = rawurlencode($parent->slug);     
        }
        $update = "UPDATE items SET relative_path_cache = '" . ltrim(implode($names, "/"), '/') . "', relative_url_cache = '" . ltrim(implode($slugs, "/"), '/') . "' WHERE id = " . $item->id;
        print '** OLD SCHOOL ** ' . $update . "\n";
        mysql_query($update);

      } else {
        // Use the parent values to calculate the child values 
        $update = "UPDATE items3 SET relative_path_cache = '" . $item->ppath . "/" . urldecode($item->name) . "', relative_url_cache = '" . $item->purl . "/" . urldecode($item->slug) . "' WHERE id = " . $item->id;
        print $update . "\n";
        mysql_query($update);
      }


    } 
?>